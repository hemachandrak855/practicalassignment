import React from 'react';
import { } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import HomeStackNavigator from './src/navigation/home_stack_navigator';

const App = () => {
  return (
    <NavigationContainer>
      <HomeStackNavigator />
    </NavigationContainer>
  );
};

export default App;
