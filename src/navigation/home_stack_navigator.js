import React from 'react';
import { } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { IconButton } from 'react-native-paper';

import ScreenOne from '../screens/screen_one';
import ScreenTwo from '../screens/screen_two';
import ScreenThree from '../screens/screens_three';

const Stack = createStackNavigator();

const HomeStackNavigator = ({ navigation }) => {
    return (
        <Stack.Navigator
            screenOptions={{
                title: 'Assigned to Me',
                headerStyle: {
                    backgroundColor: '#f1aa47'
                }
            }}
            initialRouteName={'one'}
        >
            <Stack.Screen name="one" component={ScreenOne} options={{}} />
            <Stack.Screen name="two" component={ScreenTwo} />
            <Stack.Screen name="three" component={ScreenThree} />
        </Stack.Navigator>
    );
};

export default HomeStackNavigator;