import React, { useState } from 'react';
import { Modal, SafeAreaView, View, Text, StyleSheet, FlatList, Pressable, Dimensions } from 'react-native';
import { List, Divider, Button } from 'react-native-paper';

const screenHeight = Dimensions.get('window').height;
const testdata = ['First', 'Second', 'Third', 'Fourth', 'Fifth', 'Sixth']


const DropDownComponant = ({ visible = false, data = [], selectedItem, closeModal }) => {

    const closeModalWithSelectedItem = (item) => {
        selectedItem(item)
    }

    return (
        <Modal
            animationType={'slide'}
            transparent={true}
            visible={visible}
            onRequestClose={closeModal}
        >
            <View style={styles.conatiner}>
                <View style={styles.view1}>
                    <View style={styles.view2}>
                        <Text style={styles.text1}>{'CHOOSE OPTION FROM BELOW'}</Text>
                        <Button
                            labelStyle={{ fontSize: 14, fontWeight: '400', color: 'black', textTransform: 'none' }}
                            onPress={closeModal}
                        >
                            Close
                        </Button>
                    </View>

                    <FlatList
                        style={{ height: screenHeight / 2 }}
                        data={data}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => {
                            return (
                                <Pressable onPress={() => closeModalWithSelectedItem(item)}>
                                    <View>
                                        <List.Item
                                            titleStyle={{ fontSize: 16, fontWeight: '400' }}
                                            title={item}
                                            titleNumberOfLines={1}
                                            descriptionEllipsizeMode={'tail'}
                                            description={""}
                                        />
                                        <Divider />
                                    </View>
                                </Pressable>
                            )
                        }}
                    />
                </View>
            </View>
        </Modal>
    )
}

export { DropDownComponant };

const styles = StyleSheet.create({
    conatiner: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        paddingHorizontal: 20
    },
    view1: {
        backgroundColor: 'white'
    },
    view2: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#e1e2e3',
        height: 50,
        width: '100%',
        paddingLeft: 15
    },
    text1: {
        fontSize: 14,
        fontWeight: '800',
    }
})

