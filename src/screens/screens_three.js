import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import AsyncStorage from '@react-native-async-storage/async-storage';

const ScreenThree = ({ route, navigation }) => {

  const [markers, setMarkers] = useState([]);


  useEffect(() => {

    const unsubscribe = navigation.addListener('focus', () => {
      getData()
    });

    return unsubscribe;
  }, []);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@storage_Key')
      if (jsonValue != null) {
        let data = JSON.parse(jsonValue);
        console.log('getdata: ', data);
        let matkersData = [];
        data.forEach(item => {

          const latitude = item.latitude;
          const longitude = item.longitude;
          let newObj = {
            "id": item.id,
            "title": item.title,
            "desc": item.short_desc,
            "latlong": { latitude: Number(latitude), longitude: Number(longitude) }
          }
          matkersData.push(newObj)
        })

        console.log('data1: ', matkersData);
        setMarkers(matkersData);
      }
    } catch (e) {
      console.log('error: ', e)
    }
  }

  const markerClicked = () => {

  }

  return (

    <View style={styles.container}>
      <MapView
        style={styles.mapStyle}
        showsUserLocation={true}
        zoomEnabled={true}
        zoomControlEnabled={true}
        initialRegion={{
          latitude: 15.127474,
          longitude: 78.503396,
          latitudeDelta: 0.01,
          longitudeDelta: 0.01,
        }}
      >
        {markers.map((item, index) => (
          <Marker
            key={index}
            coordinate={item.latlong}
            title={item.title}
            description={item.desc}
            onPress={() => markerClicked(item)}
          />
        ))}

        {/* <Marker
          coordinate={{ latitude: 15.127474, longitude: 78.503396 }}
          title={"Home"}
          description={"My Home"}
        /> */}

      </MapView>

    </View>

  );
};

export default ScreenThree;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  mapStyle: {
    flex: 1
  }
})