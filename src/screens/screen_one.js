import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, Image, Pressable, ActivityIndicator } from 'react-native';
import { Searchbar, IconButton, List } from 'react-native-paper';
import { DropDownComponant } from '../components/drop_down_picker';
import AsyncStorage from '@react-native-async-storage/async-storage';

const ScreenOne = ({ navigation }) => {

  const [data, setData] = useState([]);
  const [sortData, setSortData] = useState([]);
  const [searchQuery, setSearchQuery] = React.useState('');
  const [modalVisible, setModalVisible] = React.useState('');
  const [filterData, setFiterData] = useState([]);
  const [isSortSelected, setSortSelected] = useState(false);
  const [isLoading, setIsLoading] = useState(true);


  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <IconButton
          icon="map-marker-radius-outline"
          color={'white'}
          size={30}
          onPress={() => navigation.navigate('three')}
        />
      )
    });
  }, [navigation]);

  useEffect(() => {
    fetch('https://run.mocky.io/v3/82f1d43e-2176-4a34-820e-2e0aa4566b5c')
      .then((response) => response.json())
      .then((json) => {
        // console.log('jsonData: ', json)

        let filterDataAry = [];
        let newJsonAry = [];
        json.forEach((item, index) => {
          if (!filterDataAry.includes(item.status)) {
            filterDataAry.push(item.status);
          }

          let newItem = item;
          newItem.isExpand = false;
          newJsonAry.push(newItem);
        });

        setData(newJsonAry);
        setFiterData(filterDataAry);
        setIsLoading(false)
        storeData(newJsonAry);
      })
      .catch((error) => {
        console.error(error)
      })
  }, [])

  const storeData = async (value) => {
    try {
      const jsonValue = JSON.stringify(value)
      await AsyncStorage.setItem('@storage_Key', jsonValue)
    } catch (e) {
      console.log('error: ', e)
    }
  }

  const onChangeSearch = query => {

    let sortData = data.filter((value, index) => {
      return value.title.includes(query) || value.subtitle.includes(query)
    })
    setSearchQuery(query)
    setSortSelected(true);
    setSortData(sortData)
  };


  const itemSelected = (text) => {

    if (text) {
      let sortData = data.filter((value, index) => {
        return value.status === text
      })
      setSortSelected(true);
      setSortData(sortData)
    }

    setModalVisible(false)
  }

  if (isLoading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="small" color="#0000ff" />
      </View>
    )
  }

  const expandItem = (index) => {

    let newData = data;
    let item = newData[index];
    item.isExpand = !item.isExpand;
    newData[index] = item;
    setData([...newData])
  }

  return (
    <View style={styles.container}>

      <DropDownComponant
        visible={modalVisible}
        data={filterData}
        closeModal={() => setModalVisible(false)}
        selectedItem={(text) => itemSelected(text)}
      />

      <View style={{ marginVertical: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
        <Searchbar
          style={{ width: '75%', height: 40 }}
          placeholder="Search"
          onChangeText={onChangeSearch}
          value={searchQuery}
        />
        <Pressable onPress={() => setModalVisible(true)}>
          <View style={{ flexDirection: 'row', alignItems: 'center', height: 40, borderRadius: 4, backgroundColor: 'orange', justifyContent: 'space-around' }}>
            <Text style={{ marginLeft: 5, color: 'white', fontSize: 14, fontWeight: '800' }}>{'Filter'}</Text>
            <IconButton
              style={{ marginLeft: 0 }}
              icon="filter-outline"
              color={'white'}
              size={20}
            />
          </View>
        </Pressable>
      </View>
      <FlatList
        data={isSortSelected ? sortData : data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => {

          const color = item.status.toLowerCase();

          return (
            <View style={styles.secondView}>

              <View style={styles.thirdView}>
                <Image
                  style={styles.image}
                  source={{ uri: 'https://images.unsplash.com/photo-1497215728101-856f4ea42174?ixid=MnwxMjA3fDF8MHxlZGl0b3JpYWwtZmVlZHwxNnx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60' }}
                />
                <View style={styles.thirdView1}>
                  <View>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{item.title}</Text>
                    <Text style={{ color: 'gray' }}>{item.subtitle}</Text>
                  </View>
                  <View style={[styles.statusVw, { backgroundColor: color }]}>
                    <Text style={{ color: 'white', fontWeight: '400', fontSize: 12 }}>{item.status}</Text>
                  </View>
                </View>
              </View>

              <View style={{ borderColor: '#e1e2e3', borderStyle: 'dotted', borderWidth: 1, borderRadius: 1 }}></View>
              <View style={styles.fourthView}>
                <Image
                  style={{ marginRight: 20 }}
                  source={require('../assets/images/calendar.png')}
                />
                <Text style={{ color: 'gray' }}>Created: <Text style={{ fontSize: 16, color: 'black' }}>{item.created}</Text></Text>
              </View>
              <View style={{ borderColor: '#e1e2e3', borderStyle: 'dotted', borderWidth: 1, borderRadius: 1 }}></View>

              {/* // short & long description view */}
              <View style={styles.fifthView}>
                <View style={[styles.fifthView1, { marginBottom: 20 }]}>
                  <Image
                    style={{ marginRight: 20, marginTop: 5 }}
                    source={require('../assets/images/product-description.png')}
                  />
                  <Text style={{ flex: 1, color: 'black', fontWeight: '900', fontSize: 14 }}>{item.short_desc}</Text>
                </View>

                <View style={[styles.fifthView1, {}]}>
                  <Image
                    style={{ marginRight: 20, marginTop: 5 }}
                    source={require('../assets/images/description.png')}
                  />
                  <Text numberOfLines={item.isExpand ? null : 2} style={{ flex: 1, color: 'black', fontWeight: '600', fontSize: 14 }}>{item.long_desc}</Text>
                </View>
              </View>

              {/* // Expand view */}
              <Pressable onPress={() => expandItem(index)}>
                <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: 30 }}>
                  <IconButton
                    icon={item.isExpand ? "chevron-up" : "chevron-down"}
                    color={'gray'}
                    size={25}
                    style={{ margin: 0, padding: 0 }}
                  />
                </View>
              </Pressable>
            </View>
          )
        }}
      />
    </View>
  );
};

export default ScreenOne;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 15
  },
  secondView: {
    flex: 1, marginBottom: 15, backgroundColor: 'white'
  },
  image: {
    height: 60, width: 60, borderRadius: 30, marginRight: 20
  },
  thirdView: {
    flexDirection: 'row', paddingVertical: 10, paddingLeft: 20,
  },
  thirdView1: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: 10,
    alignItems: 'center'
  },
  statusVw: {
    flex: 0.5,
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12.5,
    paddingHorizontal: 10
  },
  fourthView: {
    flexDirection: 'row', alignItems: 'center', paddingVertical: 10, paddingLeft: 20
  },
  fifthView: {
    flex: 1, paddingTop: 20, paddingHorizontal: 20
  },
  fifthView1: {
    flex: 1, flexDirection: 'row'
  }
})